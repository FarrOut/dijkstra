# Kiwiland Train Problem
This is a Java implementation of a solution to the Kiwiland Trains Problem.

## Problem Statement

The local commuter railroad services a number of towns in Kiwiland. Because of monetary concerns, all of the tracks are 'one-way.' That is, a route from Kaitaia to Invercargill does not imply the existence of a route from Invercargill to Kaitaia. In fact, even if both of these routes do happen to exist, they are distinct and are not necessarily the same distance!

The purpose of this problem is to help the railroad provide its customers with information about the routes. In particular, you will compute the distance along a certain route, the number of different routes between two towns, and the shortest route between two towns.

Input: A directed graph where a node represents a town and an edge represents a route between two towns. The weighting of the edge represents the distance between the two towns. A given route will never appear more than once, and for a given route, the starting and ending town will not be the same town.

Output: For test input 1 through 5, if no such route exists, output 'NO SUCH ROUTE'. Otherwise, follow the route as given; do not make any extra stops! For example, the first problem means to start at city A, then travel directly to city B (a distance of 5), then directly to city C (a distance of 4).

    The distance of the route A-B-C.
    The distance of the route A-D.
    The distance of the route A-D-C.
    The distance of the route A-E-B-C-D.
    The distance of the route A-E-D.
    The number of trips starting at C and ending at C with a maximum of 3 stops. In the sample data below, there are two such trips: C-D-C (2 stops). and C-E-B-C (3 stops).
    The number of trips starting at A and ending at C with exactly 4 stops. In the sample data below, there are three such trips: A to C (via B,C,D); A to C (via D,C,D); and A to C (via D,E,B).
    The length of the shortest route (in terms of distance to travel) from A to C.
    The length of the shortest route (in terms of distance to travel) from B to B.
    The number of different routes from C to C with a distance of less than 30. In the sample data, the trips are:

Test Input:

For the test input, the towns are named using the first few letters of the alphabet from A to D. A route between two towns (A to B) with a distance of 5 is represented as AB5.

Graph: *AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7*

Expected Output:

- Output 1: 9
- Output 2: 5
- Output 3: 13
- Output 4: 22
- Output 5: NO SUCH ROUTE
- Output 6: 2
- Output 7: 3
- Output 8: 9
- Output 9: 9
- Output 10: 7


## Design
The solution to this problem is done using a variation of Djikstra's Algorithm. Some alterations to the algorithm needed to be made to compliment this particular problem specification. The most significant being that pure Djikstra's Algorithm does not allow a route to end on the origin node. The Kiwiland Train Problem specifically asks for this.

### Djikstra's Algorithm
Let the node at which we are starting be called the initial node. Let the weight of node Y be the weight from the initial node to Y. Dijkstra's algorithm will assign some initial weight values and will try to improve them step by step.

   - Mark all nodes unvisited. Create a set of all the unvisited nodes called the unvisited set.
   - Assign to every node a tentative weight value: set it to zero for our initial node and to infinity for all other nodes. Set the initial node as node.
   - For the node node, consider all of its unvisited neighbors and calculate their tentative distances through the node node. Compare the newly calculated tentative weight to the node assigned value and assign the smaller one. For example, if the node node A is marked with a weight of 6, and the edge connecting it with a neighbor B has length 2, then the weight to B through A will be 6 + 2 = 8. If B was previously marked with a weight greater than 8 then change it to 8. Otherwise, keep the node value.
   - When we are done considering all of the unvisited neighbors of the node node, mark the node node as visited and remove it from the unvisited set. A visited node will never be checked again.
   - If the destination node has been marked visited (when planning a route between two specific nodes) or if the smallest tentative weight among the nodes in the unvisited set is infinity (when planning a complete traversal; occurs when there is no connection between the initial node and remaining unvisited nodes), then stop. The algorithm has finished.
   - Otherwise, select the unvisited node that is marked with the smallest tentative weight, set it as the new "node node", and go back to step 3.

When planning a route, it is actually not necessary to wait until the destination node is "visited" as above: the algorithm can stop once the destination node has the smallest tentative weight among all "unvisited" nodes (and thus could be selected as the next "node").

## Assumptions
I took the liberty of making some assumptions in the solving of this problem.

- The command line application reads input from 2 files. *graph.csv* & *data.csv*. graph.csv contains the Graph specification in the format of "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7".
data.csv contains input of the desired calculations to be made. 
The file input of this solution is currently quite horrendous - so please bare with me. The file must contain 1 line of Comma-Separated-Values in the following format:

    * maxstops,[minstops],[maxstops],[origin],[destination]
    * maxdistance,[maxdistance],[origin],[destination]
    * distance,[series of nodes]
    
## Running Instructions

To run this application, in the command line please navigate to the project root directory.

Enter the following:
**gradle run**

You can modify the data you wish to calculate by editing the *data.csv* file.

## Unit Tests

You can perform the unit tests by entering:

**gradle test**