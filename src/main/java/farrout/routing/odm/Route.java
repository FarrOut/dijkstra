package farrout.routing.odm;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * An sequenced series of Nodes.
 *
 * @author Greg Farr
 */
public class Route implements Iterable<Node> {

    LinkedList<Node> nodes;
    int distance;

    public Route() {
        nodes = new LinkedList<>();
        distance = 0;
    }

    /**
     * Add the weight of all Nodes in this Route to return the total distance of the Route.
     *
     * @return total distance in units.
     */
    public int getDistance() {
        return distance;
    }


    public boolean addNode(Node node) {
        if (!nodes.isEmpty()) {

            //Is the next node valid on the graph?
            if (!nodes.getLast().getNeighbours().containsKey(node)) {
                return false;
            }

            distance += nodes.getLast().getNeighbours().get(node);
        }

        nodes.add(node);
        return true;
    }

    public Iterable<Node> getNodes() {
        return nodes;
    }

    @Override
    public int hashCode() {
        return getId(getNodes()).hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Route) || obj == null) {
            return false;
        }

        Route other = (Route) obj;

        return Objects.equals(this.getId(this.getNodes()), getId(other.getNodes()));
    }

    private String getId(Iterable<Node> nodes) {
        StringJoiner sj = new StringJoiner("-");

        for (Node n : nodes) {
            sj.add(n.name);
        }

        return sj.toString();
    }

    @Override
    public Iterator<Node> iterator() {
        return nodes.iterator();
    }

    public Node getLast()
    {
        return nodes.getLast();
    }

    public int stops()
    {
        return nodes.size();
    }

    /**
     * An exception for if there is no route found when calculating a route between Nodes.
     */
    public class NoSuchRouteException extends Exception {
        public NoSuchRouteException() {
        }

        public NoSuchRouteException(String message) {
            super(message);
        }

        public NoSuchRouteException(String message, Throwable cause) {
            super(message, cause);
        }

        public NoSuchRouteException(Throwable cause) {
            super(cause);
        }

        public NoSuchRouteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
