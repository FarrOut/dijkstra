package farrout.routing.odm;

import farrout.data.DataSource;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

/**
 * Represents a route from an origin Node to a destination Node.
 */
public class Graph {

    // Each Node has multiple neighbouring Nodes.
    private HashMap<String, Node> routeTable;

    public Graph() {
        routeTable = new HashMap<>();
    }

    public Graph(HashMap<String, Node> routeTable) {
        this.routeTable = routeTable;
    }

    public Graph(DataSource dataSource) throws FileNotFoundException {
        this(dataSource.getRouteTable());
    }


    public HashMap<String, Node> getRouteTable() {
        return routeTable;
    }

    /**
     * Checks if the Node exists in the Graph.
     *
     * @param node
     * @return
     */
    public boolean contains(Node node) {
        return getRouteTable().containsKey(node.name);
    }
}
