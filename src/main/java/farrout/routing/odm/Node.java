package farrout.routing.odm;

import java.util.HashMap;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * A node, or vertex on the graph representing a stop.
 *
 * @author Greg Farr*
 */
public class Node {

    //Everybody needs good neighbours
    HashMap<Node, Integer> neighbours;

    //Identity of this node
    String name;


    public Node(String name) {
        this.neighbours = new HashMap<>();
        this.name = name;
    }


    @Override
    public boolean equals(Object obj) {
        Node other = (Node) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public int hashCode() {
        if (this.name == null) return 0;
        return this.name.hashCode();
    }

    @Override
    public String toString() {

        StringJoiner sj = new StringJoiner(",");

        for (Node n : neighbours.keySet()){
            sj.add(n.name);
        }

        return "Node{" +
                "neighbours=" + sj.toString() +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public HashMap<Node, Integer> getNeighbours() {
        return neighbours;
    }

    public void addNeighbour(Node neighbour, int weight) {
        getNeighbours().putIfAbsent(neighbour, weight);
    }
}
