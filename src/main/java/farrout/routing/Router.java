package farrout.routing;

import farrout.routing.odm.Graph;
import farrout.routing.odm.Node;
import farrout.routing.odm.Route;

import java.util.*;

/**
 * Worker responsible for calculating Routes and distances between Nodes.
 *
 * @author Greg Farr
 */
public class Router {

    private Graph graph;

    public Router(Graph graph) {
        this.graph = graph;
    }


    public int distanceOfRoute(List<Node> nodes) throws NoSuchRouteException {
        return buildRoute(nodes).getDistance();
    }

    /**
     * Build a Route from the Nodes given. \n\n
     * The Nodes are compared against the Graph to check if the Route exists, if not, a NoSuchRouteException is thrown.
     *
     * @param nodes
     * @return route Valid Route.
     * @throws NoSuchRouteException if the desired route does not exist in the Graph.
     */
    private Route buildRoute(List<Node> nodes) throws NoSuchRouteException {
        Route route = new Route();

        for (Node n : nodes) {

            if (!route.addNode(graph.getRouteTable().get(n.getName()))) {
                throw new NoSuchRouteException("The desired route does not exist in the Graph.");
            }
        }

        return route;
    }

    /**
     * Calculate number of Routes between two Nodes within a maximum number of stops.
     *
     * @param origin      Origin Node
     * @param destination Destination Node
     * @param minStops    Minimum stops
     * @param maxStops    Maximum stops
     * @return
     */
    public int numberOfRoutesBetween(Node origin, Node destination, int minStops, int maxStops) throws NoSuchRouteException {

        //Build up a set of routes we've explored
        HashSet<Route> routes = new HashSet<>();

        Node[] visited = new Node[maxStops + 1];
        visited[0] = origin;

        findRoutes(routes, visited, destination, 0, minStops, maxStops);

        if (routes.size() == 0) throw new NoSuchRouteException("NO SUCH ROUTE");

        return routes.size();
    }


    /**
     * Recurssive function to find all routes from the starting Node to the end Node within a maximum depth.
     *
     * @param visited list of Nodes already visited
     * @param end     possible next Node
     * @param depth   how far down into recursion are we?
     * @return
     */
	private void findRoutes(HashSet<Route> routes, Node[] visited, Node end, int depth, int minStops, int maxStops)
	{
        depth++;
        if (depth > maxStops)        //Check if depth level is within limits
            return;

        Node current = visited[depth - 1];

        //Visit your neighbours
        for (Node neightbour : current.getNeighbours().keySet()) {

            //End of a route
            //If the neighbour is the end, we're building the route.
            if (Objects.equals(neightbour, end) && depth >= minStops) {
                Route route = new Route();

                //Add all Nodes already passed
                for (int i = 0; i < depth; i++) {
					route.addNode(visited[i]);
                }

                //the neighbour that matched end still needs to be added.
                route.addNode(end);

                //Another valid route
                routes.add(route);
			}
                //We've been here
                visited[depth] = neightbour;

                findRoutes(routes, visited, end, depth, minStops, maxStops);

        }
    }

    /**
     * Pure Djiktra's Algorithm.
     *
     * @param origin
     * @param destination
     * @return
     */
    public int shortestDistance(Node origin, Node destination) {

        //Make sure we're working with the same object
        origin = graph.getRouteTable().get(origin.getName());
        destination = graph.getRouteTable().get(destination.getName());

        List<TentativeNode> nodes = new ArrayList<>();

        //Set all other nodes' distance to Infinite
        for (Node n : graph.getRouteTable().values()) {
            if (Objects.equals(n, origin)) {
                nodes.add(new TentativeNode(n, 0, false));
            } else {
                nodes.add(new TentativeNode(n, Integer.MAX_VALUE, false));
            }
        }

        //Score each neighbour
        int visited = 0;
        Route route = new Route();

        //+1 to allow start Node to be revisited
        while (visited < nodes.size() + 1) {
            //The node with the smallest distance that has NOT been visited
            Collections.sort(nodes);
            TentativeNode current = nodes.get(0);

            for (Map.Entry<Node, Integer> entry : current.node.getNeighbours().entrySet()) {
                //The neighbour
                Node key = entry.getKey();
                Integer value = entry.getValue();

                TentativeNode neighbour = getTentativeNode(nodes, key).get();

                neighbour.distance = Math.min(current.distance + value, neighbour.distance);
            }

            //Add shortest leg to route
            route.addNode(current.node);

            current.visited = true;

            visited++;
        }

        return route.getDistance();
    }


    /**
     * Find TentativeNode for a specific Node
     */
    public Optional<TentativeNode> getTentativeNode(List<TentativeNode> tentativeNodes, Node current) {
        for (TentativeNode t : tentativeNodes) {
            if (t.node == current) {
                return Optional.ofNullable(t);
            }
        }
        return Optional.empty();
    }

    private class TentativeNode implements Comparable<TentativeNode> {
        Node node;
        int distance;
        boolean visited;

        public TentativeNode(Node node, int distance, boolean visited) {
            this.node = node;
            this.distance = distance;
            this.visited = visited;
        }

        @Override
        public String toString() {
            return "TentativeNode{" +
                    "node=" + node.getName() +
                    ", distance=" + distance +
                    ", visited=" + visited +
                    '}';
        }

        @Override
        public int compareTo(TentativeNode o) {

            //If  both visited statuses are the same, sort by distances
            if (this.visited == o.visited) {
                return this.distance < o.distance ? -1 : 1;
            } else if (this.visited) {
                //If you ARE visited, go in back
                return 1;
            } else {
                //If you're not visited, go in front
                return -1;
            }
        }
    }


	public int numberOfRoutes(Node origin, Node destination, int maxDistance) throws NoSuchRouteException
	{
		//Build up a set of routes we've explored
		HashSet<Route> routes = new HashSet<>();

		Route visited = new Route();
		visited.addNode(origin); //Add our starting point

		findRoutesMaxDistance(routes, visited, destination, maxDistance);

		if (routes.size() == 0) throw new NoSuchRouteException("NO SUCH ROUTE");

		return routes.size();
    }


	/**
	 * Recurssive function to find all routes from the starting Node to the end Node within a maximum depth.
	 *
	 * @param visited list of Nodes already visited
	 * @param end     possible next Node
	 * @return
	 */
	private void findRoutesMaxDistance(HashSet<Route> routes, Route visited, Node end, int maxDistance)
	{
	    if(visited.getDistance() >= maxDistance)
        {
            return;
        }

        Node current = visited.getLast();
	    if(visited.stops() > 1 && Objects.equals(current, end))
        {
            routes.add(visited);
        }

        //Visit your neighbours
        for (Node neighbour : current.getNeighbours().keySet()) {
            Route route = new Route();
            for(Node n : visited)
            {
                route.addNode(n);
            }
            route.addNode(neighbour);
            findRoutesMaxDistance(routes, route, end, maxDistance);
        }
	}

	/**
	 * An exception for if there is no route found when calculating a route between Nodes.
     */
    public class NoSuchRouteException extends Exception {
        public NoSuchRouteException() {
        }

        public NoSuchRouteException(String message) {
            super(message);
        }

        public NoSuchRouteException(String message, Throwable cause) {
            super(message, cause);
        }

        public NoSuchRouteException(Throwable cause) {
            super(cause);
        }

        public NoSuchRouteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
