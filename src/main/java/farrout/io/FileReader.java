package farrout.io;

import java.io.File;

public class FileReader {

    /**
     * Access a file in the resources by it's filename
     *
     * @param fileName
     * @return File object.
     */
    public File readFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }
}
