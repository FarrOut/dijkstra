package farrout.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * @author Greg Farr
 * @apiNote A utility class for parsing CSV files.
 */
public class CsvParser {

    static final String delimiter = ",";
    private String fileName;

    public CsvParser() {
        //TODO protect against null fileName
    }

    public CsvParser(String fileName) {
        this.fileName = fileName;
    }

    public List<String> parseCSV() throws FileNotFoundException {
        return parseCSV(fileName);
    }

    /**
     * Parse a CSV file, which may include multiple lines, and return a flat list of values.
     *
     * @param fileName
     * @return List of values.
     */
    public List<String> parseCSV(String fileName) throws FileNotFoundException {

        if (Objects.isNull(fileName) || fileName.isEmpty()) {
            throw new FileNotFoundException("Empty filename was specified.");
        }


        farrout.io.FileReader fileReader = new farrout.io.FileReader();
        File file = fileReader.readFile(fileName);

        return parseCSV(file);
    }

    public List<String> parseCSV(File file) {
        List<String> values = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                String[] v = line.split(delimiter);

                for (String s : v) {
                    values.add(s.trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return values;
    }
}
