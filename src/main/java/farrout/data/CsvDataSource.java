package farrout.data;

import farrout.io.CsvParser;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Obtain Route data from a CSV file.
 *
 * @author Greg Farr
 */
public class CsvDataSource extends DataSource {

    CsvParser parser;

//TODO add protection for if fileName is null

    public CsvDataSource(CsvParser parser) {
        this.parser = parser;
    }

    @Override
    public List<String> getList() throws FileNotFoundException {
        return parser.parseCSV();
    }
}
