package farrout.data;

import farrout.routing.odm.Node;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * An adapter to sit between the objects that reads the text file and those that needs the data as Edge objects.
 * It allows one to plug what ever worker that collects data.
 *
 * @author Greg Farr
 */
public abstract class DataSource {

    /**
     * A List of Strings obtained in a custom way.
     *
     * @return
     */
    public abstract List<String> getList() throws FileNotFoundException;

    /**
     * Convert List of Strings from a data source to a List of Edges
     *
     * @return
     */
    private HashMap<String, Node> convertObjects() throws FileNotFoundException {
        HashMap<String, Node> nodes = new HashMap<>();

        for (String s : getList()) {

//            List<Edge> edges = new ArrayList<>();
            //TODO perhaps there's a nicer way of doing this?
            //From Node
            char from = s.charAt(0);

            //To Node
            char to = s.charAt(1);

            //Distance
            //TODO add int parse protection
            int distance = Integer.parseInt(s.substring(2));

            nodes.putIfAbsent(String.valueOf(from), new Node(String.valueOf(from)));
            nodes.putIfAbsent(String.valueOf(to), new Node(String.valueOf(to)));


            //Add this Node to the route table if it's not already.
            Node start = nodes.get(String.valueOf(from));
            Node end = nodes.get(String.valueOf(to));

            //Add weight of path to neighbour
            start.addNeighbour(end, distance);
        }

        return nodes;
    }

    /**
     * Pass List of Edge objects on.
     *
     * @return
     */
    public HashMap<String, Node> getRouteTable() throws FileNotFoundException {
        return convertObjects();
    }
}
