package farrout;

import farrout.data.CsvDataSource;
import farrout.io.CsvParser;
import farrout.routing.Router;
import farrout.routing.Router.NoSuchRouteException;
import farrout.routing.odm.Graph;
import farrout.routing.odm.Node;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Application runner for the Kiwiland train solution
 */
public class Trains {

    public static void main(String[] args) {

        //Read graph data from file
        CsvDataSource dataSource = new CsvDataSource(new CsvParser("graph.csv"));

        Graph graph = null;
        try {
            graph = new Graph(dataSource);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        Router router = new Router(graph);

        //Read data
        CsvParser parser = new CsvParser("data.csv");
        try {
            List<String> lines = parser.parseCSV();

            //Find shortest distance
            if ("distance".equalsIgnoreCase(lines.get(0))) {
                List<Node> nodes = new ArrayList<>();

                for (int i = 1; i < lines.size(); i++) {
                    String s = lines.get(i);

                    if (s == null) {
                        System.out.println("NO SUCH ROUTE");
                        System.exit(0);
                    } else {
                        Node n = graph.getRouteTable().get(s.toUpperCase());

                        if (n == null) {
                            System.out.println("NO SUCH ROUTE");
                            System.exit(0);
                        } else {
                            nodes.add(n);
                        }
                    }
                }

                int distance = router.distanceOfRoute(nodes);
                System.out.println("Distance = " + distance);
            }
            //Find number of routes between two points with a max number of stops
            else if ("maxstops".equalsIgnoreCase(lines.get(0))) {
                Node origin, destination;

                String o = lines.get(3);
                String d = lines.get(4);

                int minstops = Integer.parseInt(lines.get(1));
                int maxstops = Integer.parseInt(lines.get(2));

                if (o == null || d == null) {
                    System.out.println("NO SUCH ROUTE");
                    System.exit(0);
                } else {
                    origin = graph.getRouteTable().get(o.toUpperCase());
                    destination = graph.getRouteTable().get(d.toUpperCase());

                    if (origin == null || destination == null) {
                        System.out.println("NO SUCH ROUTE");
                        System.exit(0);
                    } else {
                        int trips = router.numberOfRoutesBetween(origin, destination, minstops, maxstops);
                        System.out.println("Number of trips = " + trips);
                    }
                }


            }
            //Find number of routes between two points with a maximum distance
            else if ("maxdistance".equalsIgnoreCase(lines.get(0))) {
                Node origin, destination;

                String o = lines.get(2);
                String d = lines.get(3);

                int maxDistance = Integer.parseInt(lines.get(1));

                if (o == null || d == null) {
                    System.out.println("NO SUCH ROUTE");
                    System.exit(0);
                } else {
                    origin = graph.getRouteTable().get(o.toUpperCase());
                    destination = graph.getRouteTable().get(d.toUpperCase());

                    if (origin == null || destination == null) {
                        System.out.println("NO SUCH ROUTE");
                        System.exit(0);
                    } else {
                        int trips = router.numberOfRoutes(origin, destination, maxDistance);
                        System.out.println("Number of trips = " + trips);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
            System.exit(1);
        } catch (NoSuchRouteException e) {
            System.out.println("NO SUCH ROUTE");
            System.exit(0);
        }

    }
}
