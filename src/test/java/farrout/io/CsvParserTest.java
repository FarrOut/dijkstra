package farrout.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CsvParserTest {

    final String fileName = "input.csv";

    CsvParser cut;

    @BeforeEach
    void setUp() {
        cut = new CsvParser();
    }

    @Test
    void testParse() throws FileNotFoundException {
        List<String> values = cut.parseCSV(fileName);

        System.out.println(Arrays.toString(values.toArray()));

        assertEquals("AB5", values.get(0));
        assertEquals("BC4", values.get(1));
        assertEquals("CD8", values.get(2));
        assertEquals("DC8", values.get(3));
        assertEquals("DE6", values.get(4));
        assertEquals("AD5", values.get(5));
        assertEquals("CE2", values.get(6));
        assertEquals("EB3", values.get(7));
        assertEquals("AE7", values.get(8));
    }
}
