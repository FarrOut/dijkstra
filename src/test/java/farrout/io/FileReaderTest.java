package farrout.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class FileReaderTest {

    final String fileName = "input.csv";

    FileReader cut;

    @BeforeEach
    void setUp() {
        cut = new FileReader();
    }

    @Test
    void testOpenFile() {
        assertNotNull(cut.readFile(fileName));
    }
}
