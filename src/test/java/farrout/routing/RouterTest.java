package farrout.routing;

import farrout.data.CsvDataSource;
import farrout.io.CsvParser;
import farrout.routing.odm.Graph;
import farrout.routing.odm.Node;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RouterTest {

    final static String fileName = "input.csv";

    private static List<String> values;

    static Graph graph;

    Router router;
    Node a, b, c, d, e;

    @BeforeAll
    static void setUp() throws FileNotFoundException {
        CsvDataSource dataSource = new CsvDataSource(new CsvParser(fileName));
        graph = new Graph(dataSource);
    }

    @BeforeEach
    void setUpEach() {
        router = new Router(graph);

        a = graph.getRouteTable().get("A");
        b = graph.getRouteTable().get("B");
        c = graph.getRouteTable().get("C");
        d = graph.getRouteTable().get("D");
        e = graph.getRouteTable().get("E");
    }


    @Test
    @DisplayName("Distance to non-existant Nodes")
    void testDistanceToNonExistingNode() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(a);
        nodes.add(new Node("Neverland"));
        nodes.add(c);

        assertThrows(Router.NoSuchRouteException.class,
                () -> {
                    router.distanceOfRoute(nodes);
                });
    }

    @Test
    void testOutput_1_distance_ABC() throws Router.NoSuchRouteException {
        List<Node> nodes = new ArrayList<>();
        nodes.add(a);
        nodes.add(b);
        nodes.add(c);

        assertEquals(9, router.distanceOfRoute(nodes));
    }

    @Test
    void testOutput_2__distance_AD() throws Router.NoSuchRouteException {
        List<Node> nodes = new ArrayList<>();
        nodes.add(a);
        nodes.add(d);

        assertEquals(5, router.distanceOfRoute(nodes));
    }

    @Test
    void testOutput_3_distance_ADC() throws Router.NoSuchRouteException {
        List<Node> nodes = new ArrayList<>();
        nodes.add(a);
        nodes.add(d);
        nodes.add(c);

        assertEquals(13, router.distanceOfRoute(nodes));
    }

    @Test
    void testOutput_4_distance_AEBCD() throws Router.NoSuchRouteException {
        List<Node> nodes = new ArrayList<>();
        nodes.add(a);
        nodes.add(e);
        nodes.add(b);
        nodes.add(c);
        nodes.add(d);

        assertEquals(22, router.distanceOfRoute(nodes));
    }

    @Test
    void testOutput_5_distance_AED() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(a);
        nodes.add(e);
        nodes.add(d);


        assertThrows(Router.NoSuchRouteException.class,
                () -> {
                    router.distanceOfRoute(nodes);
                });
    }

    @Test
    void testOutput_6_trips_CtoC() throws Router.NoSuchRouteException {
        assertEquals(2, router.numberOfRoutesBetween(c, c, 0, 3));
    }

    @Test
    void testOutput_7_trips_AtoC() throws Router.NoSuchRouteException {
        assertEquals(3, router.numberOfRoutesBetween(a, c, 4, 4));
    }


    @Test
    void testOutput_8_shortest_AC() {
        assertEquals(9, router.shortestDistance(a, c));
    }

    @Test
    void testOutput_9_shortest_BtoB() {
        assertEquals(9, router.shortestDistance(b, b));
    }

    @Test
    void testOutput_10_routes_CtoC() throws Router.NoSuchRouteException {
        assertEquals(7, router.numberOfRoutes(c, c, 30));
    }

}
