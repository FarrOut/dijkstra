package farrout.routing.odm;



import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class NodeTest {

    @Test
    void testComparison() {

        Node a = new Node("A");
        Node b = new Node("A");
        Node c = new Node("C");

        assertEquals(a, b);
        assertNotEquals(a, c);
    }
}
