package farrout.routing.odm;

import farrout.data.CsvDataSource;
import farrout.io.CsvParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GraphTest {

    final static String fileName = "input.csv";

    private static Graph graph;
    Node a, b, c, d, e;

    @BeforeAll
    static void setUpOnce() throws FileNotFoundException {
        CsvDataSource dataSource = new CsvDataSource(new CsvParser(fileName));
        graph = new Graph(dataSource);
    }

    @BeforeEach
    void setUp() {
        a = graph.getRouteTable().get("A");
        b = graph.getRouteTable().get("B");
        c = graph.getRouteTable().get("C");
        d = graph.getRouteTable().get("D");
        e = graph.getRouteTable().get("E");
    }

    @Test
    @DisplayName("Positive test if Node exists")
    void testContainsNodeTrue() {
        assertTrue(graph.contains(new Node("A")));
    }

    @Test
    @DisplayName("Negative test if Node exists")
    void testContainsNodeFalse() {
        assertFalse(graph.contains(new Node("N")));
    }

    @Test
    @DisplayName("Check on your neighbours")
    void testNeighbours() {
        a.addNeighbour(b, 5);

        assertTrue(graph.getRouteTable().containsKey(a.name));
        assertTrue(a.getNeighbours().containsKey(b));
        assertTrue(a.getNeighbours().size() == 3);
    }


    @Test
    void testNodeA() {
        a.addNeighbour(b, 5);
        a.addNeighbour(d, 5);
        a.addNeighbour(e, 7);

        assertEquals(a, graph.getRouteTable().get("A"));
    }

    @Test
    void testNodeB() {
        b.addNeighbour(c,4);

        assertEquals(b, graph.getRouteTable().get("B"));
    }

    @Test
    void testNodeC() {
        c.addNeighbour(d,8);
        c.addNeighbour(e,2);

        assertEquals(c, graph.getRouteTable().get("C"));
    }

    @Test
    void testNodeD() {
        d.addNeighbour(c,8);
        d.addNeighbour(e,6);

        assertEquals(d, graph.getRouteTable().get("D"));
    }

    @Test
    void testNodeE() {
        e.addNeighbour(b, 3);

        assertEquals(e, graph.getRouteTable().get("E"));
    }
}
